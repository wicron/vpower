#==============================================================================
# Copyright (c) 2013 vgeny Proydakov <lord.tiran@gmail.com>
#==============================================================================
# Welcome to the CMake build system for VPOWER libary.
# This is the main file where we prepare the general build environment
# and provide build configuration options.
#==============================================================================

IF(ANDROID AND WIN32)
    SET(CMAKE_MAKE_PROGRAM "$ENV{ANDROID_NDK}/prebuilt/windows/bin/make.exe")
ENDIF()

CMAKE_MINIMUM_REQUIRED(VERSION 2.8.0)

#==============================================================================

PROJECT(VPOWER)

MESSAGE(STATUS "CREATE VPOWER LIBRARY\n")
STRING(TOLOWER ${PROJECT_NAME} PROJECT_NAME_LOWER)

IF(NOT CMAKE_BUILD_TYPE)
    SET(CMAKE_BUILD_TYPE Debug)
ENDIF(NOT CMAKE_BUILD_TYPE)

IF(NOT BUILD_SHARED_LIBS)
    SET(BUILD_SHARED_LIBS ON)
ENDIF(NOT BUILD_SHARED_LIBS)

# work with cmake
SET(CMAKE_PREFIX_PATH $ENV{QTDIR}/lib/cmake)
SET(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake ${PROJECT_SOURCE_DIR})

INCLUDE(FindOS)
INCLUDE(add.boost.test)
INCLUDE(add.android.test)

IF(MSVC)
    ADD_DEFINITIONS(-D_WIN32_WINNT=0x0501)
    STRING(REGEX REPLACE /W[0-4] /W4 CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
ENDIF()

IF(PROJECT_OS_LINUX)
    SET(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -Wall -fPIC")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -fPIC -std=c++11")
ENDIF()

IF(PROJECT_OS_OSX)
    SET(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -Wall -fvisibility=hidden")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -fvisibility=hidden -fvisibility-inlines-hidden -stdlib=libc++ -std=c++11")
    SET(CMAKE_XCODE_ATTRIBUTE_GCC_VERSION "com.apple.compilers.llvm.clang.1_0")
    SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LANGUAGE_STANDARD "c++11")
    SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LIBRARY "libc++")
    SET(CMAKE_OSX_DEPLOYMENT_TARGET "10.7")
ENDIF()

IF(IOS)
    SET(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -Wall")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -stdlib=libc++ -std=c++11")
    SET(CMAKE_XCODE_ATTRIBUTE_GCC_VERSION "com.apple.compilers.llvm.clang.1_0")
    SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LANGUAGE_STANDARD "c++11")
    SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LIBRARY "libc++")
    SET(CMAKE_OSX_DEPLOYMENT_TARGET "6.0")
ENDIF()

IF(APPLE)
    SET(CMAKE_MACOSX_RPATH 1)
ENDIF()

SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -DNDEBUG -DBOOST_DISABLE_ASSERTS -DQT_NO_DEBUG")
SET(CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS_DEBUG}   -DDEBUG_MODE")

IF(MSVC)
    SET(Boost_USE_STATIC_LIBS   ON)
    SET(Boost_USE_MULTITHREADED ON)
ENDIF(MSVC)

# build directory config
SET(LIBRARY_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR})
SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR})

SET(CONFIGS_DIRECTORY ${PROJECT_SOURCE_DIR}/config)
SET(SCRIPTS_DIRECTORY ${PROJECT_SOURCE_DIR}/scripts)

SET(FILES 
    ${PROJECT_SOURCE_DIR}/FindVPOWER.cmake
    ${PROJECT_SOURCE_DIR}/README
    ${PROJECT_SOURCE_DIR}/LICENSE
    ${CONFIGS_DIRECTORY}/registers.txt
    )

FIND_PACKAGE(VCORE)
INCLUDE_DIRECTORIES(${VCORE_INCLUDE_DIRS})

FIND_PACKAGE(Boost COMPONENTS system thread chrono REQUIRED)
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})

IF(Boost_FOUND AND VCORE_FOUND)

    SET(BUILD_VPOWER TRUE)
    SET(BUILD_VPOWER_MESSAGE "Boost and VCORE found.")
    ADD_SUBDIRECTORY(${PROJECT_SOURCE_DIR}/vpower)

ELSE()

    SET(BUILD_VPOWER FALSE)

    IF(NOT Boost_FOUND)
        SET(BUILD_VPOWER_MESSAGE "${BUILD_VPOWER_MESSAGE} You need to install boost.")
    ENDIF(NOT Boost_FOUND)

    IF(NOT VCORE_FOUND)
        SET(BUILD_VPOWER_MESSAGE "${BUILD_VPOWER_MESSAGE} You need to install VCORE form http://github.com/wicron/vcore.")
    ENDIF(NOT VCORE_FOUND)

    SET(BUILD_VPOWER_UI FALSE)
    SET(BUILD_VPOWER_UI_MESSAGE "VPOWER build error.")

ENDIF(Boost_FOUND AND VCORE_FOUND)

IF(NOT DISABLE_TEST)
    INCLUDE(CTest)
    ENABLE_TESTING()
    ADD_SUBDIRECTORY(tests)
ENDIF()

#==============================================================================

IF(ANDROID)
    SET(CMAKE_INSTALL_PREFIX "${ANDROID_NDK}/platforms/android-${ANDROID_NATIVE_API_LEVEL}/arch-${ANDROID_ARCH_NAME}/usr")
ELSEIF(IOS)
    SET(CMAKE_INSTALL_PREFIX "${CMAKE_IOS_SDK_ROOT}/usr")
ELSEIF(UNIX)
    SET(CMAKE_INSTALL_PREFIX /usr/local)
ELSEIF(WIN32)
    SET(COMPILER_STRING "")
    IF(MSVC)
        SET(COMPILER_STRING "msvc")
        IF(MSVC_VERSION STREQUAL "1800")
            SET(COMPILER_VERSION "2012")
        ELSEIF(MSVC_VERSION STREQUAL "1700")
            SET(COMPILER_VERSION "2011")
        ELSEIF(MSVC_VERSION STREQUAL "1600")
            SET(COMPILER_VERSION "2010")
        ELSEIF(MSVC_VERSION STREQUAL "1500")
            SET(COMPILER_VERSION "2008")
        ELSE()
            MESSAGE(FATAL_ERROR "Bad msvc version: ${MSVC_VERSION}")
        ENDIF()
        SET(COMPILER_STRING "${COMPILER_STRING}-${COMPILER_VERSION}")
    ELSEIF(MINGW)
        SET(COMPILER_STRING "mingw")
    ELSE()
        MESSAGE(FATAL_ERROR "Bad compiler")
    ENDIF(MSVC)

    SET(TARGET_CPU "any")
    IF(CMAKE_CL_64)
        SET(TARGET_CPU "amd64")
    ELSE()
        SET(TARGET_CPU "x86")
    ENDIF()

    SET(CMAKE_INSTALL_PREFIX "c:/libraries/${PROJECT_NAME_LOWER}/${PROJECT_NAME_LOWER}-${COMPILER_STRING}-${TARGET_CPU}")
ENDIF()

#==============================================================================
#---------------------------------- info --------------------------------------
#==============================================================================

# build info
SET(BUILD_INFO_BAR "====================================================================================")
SET(NOOP_STRING "")

MESSAGE(STATUS ${NOOP_STRING})
MESSAGE(STATUS ${BUILD_INFO_BAR})
MESSAGE(STATUS "Summary of the installation:")
MESSAGE(STATUS ${BUILD_INFO_BAR})
MESSAGE(STATUS "BUILD VPOWER:            ${BUILD_VPOWER}     ${BUILD_VPOWER_MESSAGE}")
MESSAGE(STATUS ${BUILD_INFO_BAR})
MESSAGE(STATUS "CMAKE_C_COMPILER:   ${CMAKE_C_COMPILER}")
MESSAGE(STATUS "CMAKE_CXX_COMPILER: ${CMAKE_CXX_COMPILER}")
MESSAGE(STATUS ${BUILD_INFO_BAR})
STRING(STRIP ${CMAKE_C_FLAGS}       CMAKE_C_FLAGS_STR)
STRING(STRIP ${CMAKE_CXX_FLAGS}     CMAKE_CXX_FLAGS_STR)
MESSAGE(STATUS "CMAKE_C_FLAGS:      ${CMAKE_C_FLAGS_STR}")
MESSAGE(STATUS "CMAKE_CXX_FLAGS:    ${CMAKE_CXX_FLAGS_STR}")
MESSAGE(STATUS ${BUILD_INFO_BAR})
MESSAGE(STATUS "Build type : ${CMAKE_BUILD_TYPE}")
MESSAGE(STATUS ${BUILD_INFO_BAR})
MESSAGE(STATUS "Install directory : ${CMAKE_INSTALL_PREFIX}")
MESSAGE(STATUS ${BUILD_INFO_BAR})
MESSAGE(STATUS ${NOOP_STRING})

#==============================================================================
#--------------------------------install --------------------------------------
#==============================================================================

INSTALL(FILES vpower/v_power_global.h  DESTINATION include/vpower)
INSTALL(FILES vpower/v_power_adapter.h DESTINATION include/vpower)
INSTALL(FILES vpower/v_power_thread.h  DESTINATION include/vpower)

IF(MSVC)
    SET(UNINSTALL_COMMAND "UNINSTALL")
ELSE()
    SET(UNINSTALL_COMMAND "uninstall")
ENDIF()

CONFIGURE_FILE(
    "${PROJECT_SOURCE_DIR}/cmake/cmake.uninstall.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake.uninstall.cmake"
    IMMEDIATE @ONLY
    )

ADD_CUSTOM_TARGET(${UNINSTALL_COMMAND}
    COMMAND "${CMAKE_COMMAND}" -P "${CMAKE_CURRENT_BINARY_DIR}/cmake.uninstall.cmake"
    COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_INSTALL_PREFIX}/include/${PROJECT_NAME_LOWER}
    )
