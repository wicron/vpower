/*
 *  Copyright (c) 2013 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// ------------------------------------------------------------------- PLATFORM
#include <vcore/platform.h>
// ------------------------------------------------------------------- INCLUDES
#include <iostream>

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

#define BOOST_TEST_MODULE TEST_POWER_ADAPTER
#include <boost/test/included/unit_test.hpp>

#ifdef OS_WINDOWS
#   include <vcore/hal/v_serial_port_manager.h>
#endif // OS_WINDOWS

#include <vcore/utils/v_benchmark.h>
// ------------------------------------------------------------------- SYNOPSIS
#include <vpower/v_power_adapter.h>
// ----------------------------------------------------------------------------

#if defined ( OS_WINDOWS )
#   define PORT_NAME "COM6"
#elif defined ( OS_UNIX )
#   define PORT_NAME "/dev/power"
#endif

#define STRESS_TEST

// ms
const int RECONNECT_ITER = 30;
const int CYCLE_TIMEOUT  = 100;
const int SLEEP_TIMEOUT  = 750;

#ifdef STRESS_TEST
const int GET_DATA_ITER  = 5000;
#else
const int GET_DATA_ITER  = 500;
#endif

void port_not_found()
{
    std::cerr << "port : " << PORT_NAME << " not found" << std::endl;
    exit(0);
}

using namespace vpower;

boost::shared_ptr<VPowerAdapter> g_p_adapter;

BOOST_AUTO_TEST_SUITE(TEST_POWER_ADAPTER)

BOOST_AUTO_TEST_CASE(init)
{
#ifdef OS_UNIX
    bool check = boost::filesystem::exists(PORT_NAME);
    if(!check) {
        port_not_found();
    }
#endif // OS_UNIX

#ifdef OS_WINDOWS
    std::list<std::string> ports;
    vcore::VSerialPortManager::getListPorts(ports);
    auto it = std::find(ports.begin(), ports.end(), PORT_NAME);
    if(it == ports.end()) {
        port_not_found();
    }
#endif // OS_WINDOWS

    g_p_adapter = boost::shared_ptr<VPowerAdapter>(new VPowerAdapter);
    if(!g_p_adapter->connect(PORT_NAME)) {
        std::cerr << "CONNECT ERROR" << std::endl;
        exit(1);
    }
    std::cout << "CONNECT OK" << std::endl;
}

BOOST_AUTO_TEST_CASE(reconnect)
{
    for(int i = 0; i < RECONNECT_ITER; i++){
        BOOST_CHECK(g_p_adapter->reconnect());
    }
}

BOOST_AUTO_TEST_CASE(get_data)
{
    ReadData data;

    int readError = 0;

    for(int i = 0; i < GET_DATA_ITER; i++) {
        bool read = g_p_adapter->readData();
        g_p_adapter->getData(data);

        if(!read) {
            readError++;
        }

        std::cout << "iteration: " << i << " read: " << read << std::endl;
        std::cout << data << std::endl;

        boost::this_thread::sleep(boost::posix_time::milliseconds(CYCLE_TIMEOUT));
    }

    std::cout << "\n" << "read error: " << readError << std::endl;
}

BOOST_AUTO_TEST_CASE(power_off)
{
    std::cout << "read: " << g_p_adapter->readData() << std::endl;

    std::cout << "power off" << std::endl;
    g_p_adapter->powerOff(true);
    g_p_adapter->writeData();

    std::cout << "read: " << g_p_adapter->readData() << std::endl;
}

BOOST_AUTO_TEST_CASE(wake)
{
    std::cout << "read: " << g_p_adapter->readData() << std::endl;

    std::cout << "power on" << std::endl;
    g_p_adapter->powerOff(false);
    g_p_adapter->writeData();
    boost::this_thread::sleep(boost::posix_time::milliseconds(SLEEP_TIMEOUT));

    std::cout << "read: " << g_p_adapter->readData() << std::endl;
}

BOOST_AUTO_TEST_CASE(benchmark_reconnect)
{
    vcore::VBenchmark::run(boost::bind(&VPowerAdapter::reconnect, g_p_adapter), "VPowerAdapter::reconnect");
}

BOOST_AUTO_TEST_CASE(benchmark_read)
{
    vcore::VBenchmark::run(boost::bind(&VPowerAdapter::readData, g_p_adapter), "VPowerAdapter::readData");
}

BOOST_AUTO_TEST_CASE(benchmark_write)
{
    vcore::VBenchmark::run(boost::bind(&VPowerAdapter::writeData, g_p_adapter), "VPowerAdapter::writeData");
}

BOOST_AUTO_TEST_CASE(cleanup)
{
    g_p_adapter->disconnect();
}

BOOST_AUTO_TEST_SUITE_END() // TEST_POWER_ADAPTER
