/*
 *  Copyright (c) 2013 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _V_POWER_GLOBAL_H_
#define _V_POWER_GLOBAL_H_

// ------------------------------------------------------------------- PLATFORM
#include <vcore/platform.h>
// ------------------------------------------------------------------- INCLUDES
#include <map>
#include <vector>

#include <stdint.h>

#include <vcore/types.h>
#include <vcore/v_core_global.h>
// ------------------------------------------------------------------- SYNOPSIS
// ----------------------------------------------------------------------------

#ifdef VPOWER
#   define VPOWER_LIBRARY_EXPORT V_DECL_EXPORT
#else
#   define VPOWER_LIBRARY_EXPORT V_DECL_IMPORT
#endif // VPOWER

// ----------------------------------------------------------------------------

namespace vpower {

typedef std::vector<int> battery_capacity;
typedef std::vector<int> battery_status;
typedef std::vector<int> battery_charge_status;

typedef uint16_t register_type;

enum { BATTERY_NUM = 2 };
enum { DEFAULT_VALUE = 0 };

enum battery_id {
    PRIMARY   = 0,
    SECONDARY = 1
};

enum control_command {
    stop  = 13,
    start = 39,
    reset = 72
};

enum
{
    LIGHT_DISABLED = 0,
    LIGHT_ENABLED = 19
};

PACK (
struct ReadData
{
    ReadData() noexcept {
        for(size_t i = 0; i < BATTERY_NUM; i++) {
            batteryCapacity[i] = DEFAULT_VALUE;
            batteryCurrentCapacity[i] = DEFAULT_VALUE;
            batteryStatus[i] = DEFAULT_VALUE;
            batteryChargeStatus[i] = DEFAULT_VALUE;
        }
    }

    register_type light = LIGHT_DISABLED;
    register_type batteryCapacity[2];
    register_type batteryCurrentCapacity[2];
    register_type batteryStatus[2];
    register_type batteryChargeStatus[2];
    register_type shutdownStatus = DEFAULT_VALUE;
});

PACK (
struct WriteData
{
    register_type light = LIGHT_DISABLED;
    register_type power = DEFAULT_VALUE;
    register_type control = DEFAULT_VALUE;
});

} // vpower

VPOWER_LIBRARY_EXPORT std::ostream& operator<<(std::ostream &os, const vpower::ReadData& data) noexcept;

#endif // _V_POWER_GLOBAL_H_
