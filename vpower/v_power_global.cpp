/*
 *  Copyright (c) 2013 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// ------------------------------------------------------------------- INCLUDES
#include <iostream>
// ------------------------------------------------------------------- SYNOPSIS
#include <vpower/v_power_global.h>
// ----------------------------------------------------------------------------

std::ostream & operator<<(std::ostream &os, const vpower::ReadData& data) noexcept
{
    os << "battery max capacity: ";
    size_t n;
    for( n = 0; n < vpower::BATTERY_NUM; n++) {
        os << data.batteryCapacity[n] << " ";
    }
    os << std::endl;
    os << "battery current capacity: ";
    for( n = 0; n < vpower::BATTERY_NUM; n++) {
        os << data.batteryCurrentCapacity[n] << " ";
    }
    os << std::endl;
    os << "battery status: ";
    for( n = 0; n < vpower::BATTERY_NUM; n++) {
        os << data.batteryStatus[n] << " ";
    }
    os << std::endl;
    os << "battery charge status: ";
    for( n = 0; n < vpower::BATTERY_NUM; n++) {
        os << data.batteryChargeStatus[n] << " ";
    }
    os << std::endl;

    return os;
}
