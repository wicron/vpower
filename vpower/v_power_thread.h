/*
 *  Copyright (c) 2013 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _V_POWER_THREAD_H_
#define _V_POWER_THREAD_H_

// ------------------------------------------------------------------- INCLUDES
#include <vcore/v_core_global.h>
#include <vcore/cs/v_device_thread.h>
// ------------------------------------------------------------------- SYNOPSIS
#include <vpower/v_power_global.h>
#include <vpower/v_power_adapter.h>
// ----------------------------------------------------------------------------

namespace vpower {

class VPOWER_LIBRARY_EXPORT VPowerThread : public vcore::VDeviceThread<VPowerAdapter, ReadData>
{
public:
    bool getShutdownStatus(bool& shutdown) noexcept;
    bool getBatteryCapacity(battery_capacity& data) noexcept;
    bool getBatteryCurrentCapacity(battery_capacity& data) noexcept;
    bool getBatteryStatus(battery_status& data) noexcept;
    bool getBatteryChargeStatus(battery_charge_status& data) noexcept;
    bool getLightState(bool& enabled) noexcept;

    bool control(control_command command) noexcept;
    bool powerOff(bool state) noexcept;
    bool newBattery(battery_id id) noexcept;
    bool enableLight( bool enable ) noexcept;
};

} // vpower

#endif // _V_POWER_THREAD_H_
