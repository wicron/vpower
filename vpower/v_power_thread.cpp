/*
 *  Copyright (c) 2012 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "v_power_thread.h"

#include <cassert>
#include <mutex>

using namespace vpower;

bool VPowerThread::getShutdownStatus(bool& shutdown) noexcept
{
    std::unique_lock<boost::mutex> locker(*getRMutex());
    shutdown = getBuffer()->shutdownStatus == VPowerAdapter::SHUTDOWN;
    return working();
}

bool VPowerThread::getBatteryCapacity(battery_capacity& data) noexcept
{
    data.resize(BATTERY_NUM);
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    for(size_t i = 0; i < BATTERY_NUM; ++i) {
        data[i] = buffer->batteryCapacity[i];
    }
    return working();
}

bool VPowerThread::getBatteryCurrentCapacity(battery_capacity& data) noexcept
{
    data.resize(BATTERY_NUM);
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    for(size_t i = 0; i < BATTERY_NUM; ++i) {
        data[i] = buffer->batteryCurrentCapacity[i];
    }
    return working();
}

bool VPowerThread::getBatteryStatus(battery_status& data) noexcept
{
    data.resize(BATTERY_NUM);
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    for(size_t i = 0; i < BATTERY_NUM; ++i) {
        data[i] = buffer->batteryStatus[i];
    }
    return working();
}

bool VPowerThread::getBatteryChargeStatus(battery_charge_status& data) noexcept
{
    data.resize(BATTERY_NUM);
    auto buffer = getBuffer();
    std::unique_lock<boost::mutex> locker(*getRMutex());
    for(size_t i = 0; i < BATTERY_NUM; ++i) {
        data[i] = buffer->batteryChargeStatus[i];
    }
    return working();
}

bool VPowerThread::getLightState(bool& enabled) noexcept
{
    std::unique_lock<boost::mutex> locker(*getRMutex());
    enabled = getBuffer()->light != vpower::LIGHT_DISABLED;
    return working();
}

bool VPowerThread::newBattery(battery_id id) noexcept
{
    std::unique_lock<boost::mutex> locker(*getWMutex());
    return getAdapter()->newBattery(id);
}

bool VPowerThread::enableLight( bool enable ) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->enableLight( enable );
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VPowerThread::control(control_command command) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->control(command);
        write();
    }
    notifyDataChanged( );
    return working();
}

bool VPowerThread::powerOff(bool state) noexcept
{
    {
        std::unique_lock<boost::mutex> locker(*getWMutex());
        getAdapter()->powerOff(state);
        write();
    }
    notifyDataChanged( );
    return working();
}
