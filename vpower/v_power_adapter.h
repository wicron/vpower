/*
 *  Copyright (c) 2013 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _V_POWER_ADAPTER_H_
#define _V_POWER_ADAPTER_H_

#include <memory>
#include <vcore/hal/v_modbus_rtu_master.h>

#include <vpower/v_power_global.h>

namespace vpower {
struct ReadData;
struct WriteData;
}

namespace vpower {

class VPOWER_LIBRARY_EXPORT VPowerAdapter : public vcore::VModbusRTUMaster
{
public:
    enum battery_status {
        LOW_POWER    = 0,
        HIGH_VOLTAGE = 1,
        NORMAL       = 13,
        MISSING      = 19
    };

    enum charge_status {
        CHARGED    = 255,
        DISCHARGED = 219
    };

    enum shutdown_status {
        SHUTDOWN     = 219,
        NOT_SHUTDOWN = 139
    };

public:
    VPowerAdapter() noexcept;
    ~VPowerAdapter() override;

    bool connect(const std::string& portName) noexcept;
    void disconnect() override;

    bool readData();
    bool writeData();

    const std::string& getPortName() const noexcept;
    void getData(ReadData& data) const noexcept;

    void control(control_command command) noexcept;
    void powerOff(bool state) noexcept;
    bool newBattery(battery_id id) noexcept;
    bool bootloader() noexcept;
    void enableLight( bool enable ) noexcept;

private:
    enum READ_DATA_REGISTERS { READ_START_REGISTER = 1097,
                               READ_END_REGISTER   = 1106,
                               READ_DATA_LENGHT = READ_END_REGISTER - READ_START_REGISTER + 1 };

    enum WRITE_DATA_REGISTERS { WRITE_START_REGISTERS = 1164,
                                WRITE_END_REGISTERS   = 1166,
                                WRITE_DATA_LENGHT =  WRITE_END_REGISTERS - WRITE_START_REGISTERS + 1 };

    enum { BOARD_ID = 0x01  };
    enum { BAUDRATE = 38400 };
    enum { DATA_BIT = 8     };
    enum { STOP_DIR = 1     };

    std::shared_ptr<vpower::ReadData>  m_read_data;
    std::shared_ptr<vpower::WriteData> m_write_data;
    std::string m_portName;

    static const char PARITY;
};

} // vpower

#endif // _V_POWER_ADAPTER_H_
