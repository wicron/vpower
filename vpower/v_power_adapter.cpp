/*
 *  Copyright (c) 2013 Evgeny Proydakov <lord.tiran@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "v_power_global.h"
#include "v_power_adapter.h"

#include <cassert>

enum {
    POWER_OFF = 219,
    POWER_ON  = 139
};

enum { BOOT_LOADER_ADRESS = 1163 };
enum { BOOT_LOADER_KEY    = 219 };

enum { NEW_BATTERY_ADRESS = 1167 };
enum { NEW_BATTERY_KEY    = 13 };

using namespace vpower;

const char VPowerAdapter::PARITY = 'E';

VPowerAdapter::VPowerAdapter() noexcept :
    VModbusRTUMaster()
{
    m_read_data.reset( new vpower::ReadData( ) );
    m_write_data.reset( new vpower::WriteData( ) );

    static_assert( VPowerAdapter::WRITE_DATA_LENGHT * sizeof( register_type ) == sizeof( WriteData ), "Wrong size of WriteData" );
    static_assert( VPowerAdapter::READ_DATA_LENGHT * sizeof( register_type ) == sizeof( ReadData ), "Wrong size of ReadData" );
}

VPowerAdapter::~VPowerAdapter()
{
}

bool VPowerAdapter::connect(const std::string& portName) noexcept
{
    bool res = VModbusRTUMaster::connect(portName.c_str(), BAUDRATE, PARITY, DATA_BIT, STOP_DIR);
    VModbusRTUMaster::setSlaveId(BOARD_ID);
    m_portName = portName;
    return res;
}

void VPowerAdapter::disconnect()
{
    VModbusRTUMaster::disconnect();
}

const std::string& VPowerAdapter::getPortName() const noexcept
{
    return m_portName;
}

bool VPowerAdapter::readData()
{
    return VModbusRTUMaster::readRegisters(READ_START_REGISTER, READ_DATA_LENGHT, reinterpret_cast<uint16_t*>(m_read_data.get()));
}

bool VPowerAdapter::writeData()
{
    return VModbusRTUMaster::writeRegisters(WRITE_START_REGISTERS, WRITE_DATA_LENGHT, reinterpret_cast<uint16_t*>(m_write_data.get()));
}

void VPowerAdapter::getData(vpower::ReadData& data) const noexcept
{
    data = *m_read_data.get();
}

void VPowerAdapter::control(control_command command) noexcept
{
    m_write_data->control = command;
}

void VPowerAdapter::powerOff(bool state) noexcept
{
    m_write_data->power = state ? POWER_OFF : POWER_ON;
}

void VPowerAdapter::enableLight( bool enable ) noexcept
{
    m_write_data->light = enable ? LIGHT_ENABLED : LIGHT_DISABLED;
}

bool VPowerAdapter::newBattery(battery_id id) noexcept
{
    assert(id >= 0 && id < (int)BATTERY_NUM);

    if(!VModbusRTUMaster::isConnected()) {
        return false;
    }
    try {
        uint16_t writeValue = NEW_BATTERY_KEY;
        return VModbusRTUMaster::writeRegisters(NEW_BATTERY_ADRESS + id, 1, &writeValue);
    }
    catch (...) {
    }
    return false;
}

bool VPowerAdapter::bootloader() noexcept
{
    if(!VModbusRTUMaster::isConnected()) {
        return false;
    }

    try {
        uint16_t writeValue = BOOT_LOADER_KEY;
        return VModbusRTUMaster::writeRegisters(BOOT_LOADER_ADRESS, 1, &writeValue);
    }
    catch (...) {
    }
    return false;
}
