#==============================================================================
# Copyright (c) 2013 Evgeny Proydakov <lord.tiran@gmail.com>
#
#  VPOWER_FOUND - system has VPOWER
#  VPOWER_INCLUDE_DIRS - the VPOWER include directory
#  VPOWER_LIBRARIES - link these to use VPOWER
#
#==============================================================================

IF(VPOWER_INCLUDE_DIRS AND VPOWER_LIBRARIES)

  # in cache already
  SET(VPOWER_FOUND TRUE)
  MESSAGE(STATUS "Found VPOWER: ${VPOWER_LIBRARIES}")

ELSE()

    FIND_PATH(VPOWER_INCLUDE_DIRS vpower
        PATHS
        /usr/local/include
        $ENV{VPOWER_DIRECTORY}/include
        )

    FIND_LIBRARY(VPOWER_LIBRARY_DEBUG
        NAMES vpowerd
        PATHS
        /usr/local/lib
        $ENV{VPOWER_DIRECTORY}/lib
        )

    FIND_LIBRARY(VPOWER_LIBRARY_RELEASE
        NAMES vpower
        PATHS
        /usr/local/lib
        $ENV{VPOWER_DIRECTORY}/lib
        )

    SET(VPOWER_FOUND TRUE)

    IF(NOT VPOWER_INCLUDE_DIRS)
        SET(VPOWER_FOUND FALSE)
    ENDIF(NOT VPOWER_INCLUDE_DIRS)

    IF(NOT VPOWER_LIBRARY_DEBUG  AND  NOT VPOWER_LIBRARY_RELEASE)
        SET(VPOWER_FOUND FALSE)
    ELSEIF(NOT VPOWER_LIBRARY_DEBUG)
        SET(VPOWER_LIBRARY_DEBUG ${VPOWER_LIBRARY_RELEASE})
    ELSEIF(NOT VPOWER_LIBRARY_RELEASE)
        SET(VPOWER_LIBRARY_RELEASE ${VPOWER_LIBRARY_DEBUG})
    ENDIF()

    SET(VPOWER_LIBRARIES "debug" ${VPOWER_LIBRARY_DEBUG} "optimized" ${VPOWER_LIBRARY_RELEASE})

    MARK_AS_ADVANCED(VPOWER_INCLUDE_DIRS VPOWER_LIBRARIES)

ENDIF()
